import Vue from 'vue'
import vuex from 'vuex'
import mutations from './mutation'
import getters from './getter'
import actions from './actions'
import Allsections from "./sections";

Vue.use(vuex);

const store=new vuex.Store({
    state:{ 
        endpoint:"https://api.test.leffit.nl/api/v1/leffit/newsletters/",
        templateModal:false,
        templateViewData:Allsections,
        template:{name:''},
        newsletters:[],
        sections:[],
        changesMade:false,
        activeSection:{
            id:0,title:'',image:'',content:''
        },
        loader:false,
        formErrors:[],
        
    },
    mutations,
    getters,
    actions
})


export default store