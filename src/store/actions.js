import axios from 'axios'

export default{
    get(context, endpoint){ 
        return new Promise((resolve,reject)=>{
            axios.get(endpoint)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                // console.log('error', error.request); 
        
                reject(error)
            })
        });
    },

    post(context , payload){ 
        
        return new Promise((resolve,reject)=>{
            axios.post(payload.endpoint , payload.details,{
                // onUploadProgress: function( progressEvent ) {
                //     context.state.requestStatus.perc = parseInt( Math.round( ( progressEvent.loaded * 100 ) / progressEvent.total ) );
                //     // console.log(pro);
                //   }.bind(this)
            })
            .then((data)=>{
            
                resolve(data)
            })
            .catch((error)=>{
                reject(error)
            })
        });
    },

    put(context , data){

        return new Promise((resolve,reject)=>{
            axios.put(data.endpoint , data.details)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                reject(error)
            })
        });
    },

    delete(context , endpoint){
        
        return new Promise((resolve,reject)=>{
            axios.delete(endpoint)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                reject(error)
            })
        });
    },
}