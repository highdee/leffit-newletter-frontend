export default `
<section class="four bg-white container ">
<div class="row pt-4 mb-3">
    <div class="col-md-6">
        <h2 class="h2">++{{section9header}}</h2>
        <h6 class="p">++{{section9Text1}}</h6>
    </div>
    <div class="col-md-6 text-center">
        <img src="++{{section9Image}}">
    </div>
</div>
</section>
`