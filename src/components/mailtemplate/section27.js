export default `
<section class="four bg-white container ">
<div class="row pt-4 mb-3">
    <div class="col-sm-12 container ">
        <h4 class="pt-3 four_o carry">++{{section27header1}}</h4>
        <!-- <img src="full_bg.png"> -->			
    </div>

    <div class="col-sm-6 mt-2">
        <h6 class="p">++{{section27Text1}}</h6>
    </div>
    <div class="col-sm-6 mt-2">
        <h6 class="p">++{{section27Text2}}</h6>
    </div>
</div>
</section>
`