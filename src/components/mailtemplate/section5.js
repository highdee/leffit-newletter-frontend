export default `
<section class="five bg-white container center">
<h2 class="h2 pt-3">++{{section5header}}</h2>
<h6 class="p mb-3">
    ++{{section5Text1}}
</h6>
<a href="++{{section5Link1}}" class="btn btn-lg pl-4 pr-4 pt-2 pb-2 mb-4 mt-2">++{{section5Text2}}</a>
</section>
`