export default `
    <section class="five bg-white container">
    <h6 class="p mb-3 pt-4">++{{section3Text1}}</h6>
    <a href="++{{section3Link1}}" class="btn btn-lg pl-4 pr-4 pt-2 pb-2 mb-4 mt-2 button">++{{section3Text2}}</a>
    </section>
`;