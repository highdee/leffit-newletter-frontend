export default `
<section class="six bg-white text-center pb-3" >
<div class="container">
    <h2 class="h2 pt-3">++{{section24header}}</h2>
    <div class="row mt-3">
        <div class="col-sm-4 col-lg-4" >
            <img src="++{{section24Image1}}">
            <button class="btn pad btn-md mb-4 mt-3">++{{section24Text1}} </button>
            <!-- <a href="++{{section24header}}" class="btn pad btn-lg mb-4 mt-3">button met een </a> -->
        </div>
        <div class="col-sm-4 col-lg-4">
            <img src="++{{section24Image2}}">
            <button class="btn pad btn-md mb-4 mt-3">++{{section24Text2}}</button>
        </div>
        <div class="col-sm-4 col-lg-4">
            <img src="++{{section24Image3}}">
            <button class="btn pad btn-md mb-4 mt-3">++{{section24Text3}} </button>
        </div>
    </div>
</div>
</section>

`
